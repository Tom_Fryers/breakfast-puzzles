# Breakfast Puzzles

This is a script for laying out a selection of puzzles from Simon
Tatham’s Portable Puzzle Collection onto a page.

It outputs PostScript to a file, which can then be printed out.

To use it, ensure you have
[the puzzle collection](https://www.chiark.greenend.org.uk/~sgtatham/puzzles/)
installed, then run `breakfast-puzzles FILENAME`.

The supported puzzles are:

- Bridges
- Dominosa
- Filling
- Galaxies
- Keen
- Keen Multiply
- Killer
- Light Up
- Magnets
- Map
- Pattern
- Pearl
- Range
- Rectangles
- Signpost
- Singles
- Slant
- Sudoku
- Sudoku X
- Jigsaw
- Jigsaw Killer
- Tents
- Towers
- Tracks
- Unequal
- Adjacent
- Unruly

---

Breakfast Puzzles
Copyright (C) 2021 Tom Fryers

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.
